__author__ = 'hward'

from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Request
from scrapy.utils.url import urljoin_rfc
from scrapy.utils.response import get_base_url

from ..items import GameLog
import datetime

class GamesLogsSpider(BaseSpider):
    name = "gameslog"
    allowed_domains = ["cbssports.com"]
    start_urls = [
        "http://www.cbssports.com/nhl/teams/roster/DET/detroit-red-wings",
        "http://www.cbssports.com/nhl/teams/roster/NJ/new-jersey-devils",
    ]


    def parse(self, response):

        hxs = HtmlXPathSelector(response)

        for uri in hxs.select('//a[contains(@href, "/nhl/players/")]/@href').extract():
            yield Request(urljoin_rfc(get_base_url(response), uri), callback=self.get_player_profile)


    def get_player_profile(self, response):
        hxs = HtmlXPathSelector(response)

        for uri in hxs.select('//a[contains(@href, "/nhl/players/player/gamelogs")]/@href').extract():
            yield Request(urljoin_rfc(get_base_url(response), uri), callback=self.get_player_game_logs)


    def get_player_game_logs(self, response):

        hxs = HtmlXPathSelector(response)

        team = hxs.select('//h3/a/text()').extract()[0]
        number, first_name, last_name, position = hxs.select('//h1/text()').extract()[0].split() #Returns [u'8', u'Justin', u'Abdelkader,', u'LW']
        year = hxs.select('//a[@class="optsel  "]/text()').extract()[0]
        year1 , year2 = '20' + year[2:4], '20' + year[-2:]
        rows = hxs.select('//tr[@class="row1" or @class="row2"]')

        items = []
        for row in rows:
            data = row.select('td/text()').extract()

            item = GameLog()
            # Common attributes
            item['team'] = team
            item['number'] = number
            item['first_name'] = first_name
            item['last_name'] = last_name[0:-1] # Because of comma in last name
            item['position'] = position

            # Details

            # Datetime parsing
            day = int(data[0][-2:])
            month = int(data[0][0:2])
            if month > 6:
                year = int(year1)
            else:
                year = int(year2)
            item['date'] = datetime.date(year, month, day)


            item['team'] = data[1]
            item['opponent'] = data[2]
            item['result'] = data[3][0]
            item['score_team'] = data[3][1:].strip().split('-')[0]
            item['score_opponent'] = data[3][1:].strip().split('-')[1]

            item['goals'] = int(data[4])
            item['assists'] = int(data[5])
            item['points'] = int(data[6])
            item['plus_minus'] = int(data[7])
            item['ppg'] = int(data[8])
            item['shg'] = int(data[9])
            item['sog'] = int(data[10])
            item['spct'] = float(data[11])
            item['pim'] = int(data[12])
            item['mjr'] = int(data[13])
            item['msc'] = int(data[14])
            item['mnr'] = int(data[15])


            """ Extract time values """
            def time_to_seconds(time_format):
                """
                    Returns total seconds in integer
                    time_format is string e.g. 12:44 is 12 minutes and 44 seconds
                """
                min, sec = time_format.split(':')
                return 60*int(min) + int(sec)

            item['toi'], item['pp'], item['sh'] = map(time_to_seconds, [data[16], data[17], data[18]])

            items.append(item)

        return items
