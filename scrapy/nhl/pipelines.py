# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html


from webapp.players.models import GameStats

from pymongo import Connection

db = Connection('ds031417.mongolab.com:31417', 31417).lidstrom

db.authenticate('franzen', 'datsyuk')



class PlayerGameStatsPipeline(object):



    def __init__(self):
        """
            Drop GameStats database so we don't have duplicates
        """
        coll = db['players_gamestats']
        coll.remove()


    def process_item(self, item, spider):

        gs = GameStats(**dict(item))

        gs.save()

        return gs
