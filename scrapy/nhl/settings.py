# Scrapy settings for nhl project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'nhl'
BOT_VERSION = '1.0'

SPIDER_MODULES = ['nhl.spiders']
NEWSPIDER_MODULE = 'nhl.spiders'
USER_AGENT = '%s/%s' % (BOT_NAME, BOT_VERSION)


ITEM_PIPELINES = [
    'nhl.pipelines.PlayerGameStatsPipeline',
]


import os

PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../webapp'))

# Include django models to save models in pipelines.py
def setup_django_env(path):
    import imp, os
    from django.core.management import setup_environ

    f, filename, desc = imp.find_module('settings', [path])
    project = imp.load_module('settings', f, filename, desc)

    setup_environ(project)

setup_django_env(PROJECT_ROOT)