# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class GameLog(Item):
    # define the fields for your item here like:
    team = Field()
    number = Field()
    first_name = Field()
    last_name = Field()
    position = Field()

    date = Field()
    team = Field()
    opponent = Field()
    result = Field()
    score_team = Field()
    score_opponent = Field()

    goals = Field()
    goals_against = Field()
    assists = Field()
    points = Field()
    plus_minus = Field()
    ppg = Field()
    shg = Field()
    sog = Field()
    spct = Field()
    pim = Field()
    mjr = Field()
    msc = Field()
    mnr = Field()
    toi = Field()
    pp = Field()
    sh = Field()
