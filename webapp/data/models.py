__author__ = 'hward'

import pandas
import dateutil

def getDataframe():

    df = pandas.read_csv('sample.csv')

    df.index = [dateutil.parser.parse(d) for d in df.pop('timestamp')]

    return df


