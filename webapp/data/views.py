__author__ = 'hward'

# Create your views here.

from django.views.generic import TemplateView
from models import getDataframe

from utils import data

class DataframeView(TemplateView):

    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(DataframeView, self).get_context_data(**kwargs)

        df = getDataframe()

        amcharts_data = data.convertDataframeToAmchart(df)

        context['df'] = amcharts_data

        return context