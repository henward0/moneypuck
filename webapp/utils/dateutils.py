__author__ = 'hward'

from datetime import datetime

class cdatetime(datetime):

    def __repr__(self):
        return "new Date(%s, %s, %s)" % (self.year, self.month, self.day)