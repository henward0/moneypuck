__author__ = 'hward'

import pandas
import numpy as np

def create_series(queryset, field, accumulate=True):
    """
        This takes a queryset of object and returns a pandas.Series object
        of the field.
        The objects must have a date field and the field argument must exist on the object.

        Accumulate adds the fields for ongoing values overtime

    """

    li = queryset.values('date', field)

    values = np.array([x['plus_minus'] for x in li])
    index = [x['date'] for x in li]

    if accumulate:
        values = np.add.accumulate(values)

    series = pandas.Series(values, index)

    return series


def convertDataframeToAmchart(df):

    from utils.dateutils import cdatetime

    df['date'] = [cdatetime(dt.year,dt.month,dt.day) for dt in df.index]

    amchartsData = [y.to_dict() for x, y in df.iterrows()]

    df.pop('date')

    return amchartsData
