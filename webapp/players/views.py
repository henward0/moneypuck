# Create your views here.

from models import GameStats
from utils import data
from django.views.generic import TemplateView

class ChartView(TemplateView):

    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(ChartView, self).get_context_data(**kwargs)

        qs = GameStats.objects.filter(last_name="White")

        context['data'] = data.create_series(qs, 'plus_minus')
        context['player'] = "White"

        return context