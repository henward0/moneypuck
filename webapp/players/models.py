from django.db import models

# Create your models here.


class GameStats(models.Model):
    # define the fields for your item here like:
    team = models.CharField(max_length=30)
    number = models.IntegerField()
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    position = models.CharField(max_length=2)

    date = models.DateField()
    team = models.CharField(max_length=30)
    opponent = models.CharField(max_length=30)
    result = models.CharField(max_length=1)
    score_team = models.IntegerField()
    score_opponent = models.IntegerField()

    goals = models.IntegerField()
    assists = models.IntegerField()
    points = models.IntegerField()
    plus_minus = models.IntegerField()
    ppg = models.IntegerField()
    shg = models.IntegerField()
    sog = models.IntegerField()
    spct = models.FloatField()
    pim = models.IntegerField()
    mjr = models.IntegerField()
    msc = models.IntegerField()
    mnr = models.IntegerField()
    toi = models.IntegerField()     # seconds
    pp = models.IntegerField()      # seconds
    sh = models.IntegerField()      # seconds

